﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Juego : MonoBehaviour
{
    public Ficha[] fichasDeJuego;    
    public ConfiguracionDelJuego configuracionDelJuego;

    private JugadorEnemigo ia;
    private ControladorDelUI contolUI;
    private ControladorDeConexion conexion;

    public Image imagenDeTurno1;
    public Image imagenDeTurno2;

    public int turno;
    public enum tipoDeJuego { vsIA = 0, multiJugador = 1 }
    public tipoDeJuego tipoDeJuegoAJugar;

    // Use this for initialization
    void Start()
    {
        conexion = GameObject.Find("Conexion").GetComponent<ControladorDeConexion>();
        contolUI = GameObject.Find("ControladorUI").GetComponent<ControladorDelUI>();
        ia = GameObject.Find("JugadorEnemigo").GetComponent<JugadorEnemigo>();
        configuracionDelJuego.configuracionPorDefecto();               
    }

    public void iniciarJuego()
    {
        reiniciarJuego();
        if (tipoDeJuegoAJugar == tipoDeJuego.vsIA)
        {
            if (configuracionDelJuego.ia.juegaPrimero)
            {
                juegaLaIa();
            }
            else
            {
                asignarColorAlTurno(configuracionDelJuego.jugardor1.colorDelJugador);
            }
        }

        if (tipoDeJuegoAJugar == tipoDeJuego.multiJugador)
        {
            if (configuracionDelJuego.jugardor1.juegaPrimero)
            {                
                asignarColorAlTurno(configuracionDelJuego.jugardor1.colorDelJugador);
            }
            else
            {
                asignarColorAlTurno(configuracionDelJuego.jugardor2.colorDelJugador);
            }
        }
    }

    void asignarColorAlTurno(Color colorDelTurno)
    {
        imagenDeTurno1.color = colorDelTurno;
        imagenDeTurno2.color = colorDelTurno;
    }

    public void activarModoMultiJugador()
    {
        tipoDeJuegoAJugar = tipoDeJuego.multiJugador;
    }

    public void activarModoVsIA()
    {
        tipoDeJuegoAJugar = tipoDeJuego.vsIA;
    }

    public void asignarModoDeJuego(tipoDeJuego tipoDeJuegoSeleccionado)
    {
        tipoDeJuegoAJugar = tipoDeJuegoSeleccionado;
    }

    void seleccionarPrimerJugador()
    {
        int temp = Random.Range(1, 10);
        if (temp % 2 == 0)
        {
            asignarPrimerJugador(true);
        }
        else
        {
            asignarPrimerJugador(false);
        }
    }

    void asignarPrimerJugador(bool primerJugador)
    {
        configuracionDelJuego.jugardor1.juegaPrimero = primerJugador;
        configuracionDelJuego.jugardor2.juegaPrimero = !primerJugador;
    }

    void juegaLaIa()
    {
        Invoke("jugadaDeLaIa", 1);
    }

    void jugadaDeLaIa()
    {
        ia.jugar(turno);
    }      

    void reiniciarJuego()
    {
        turno = 1;
        seleccionarPrimerJugador();
        foreach (Ficha ficha in fichasDeJuego)
        {
            ficha.reiniciarFicha();
        }

        conexion.generarIndiceAleatorio();
    }

    public void activarFicha(Ficha fichaJugada)
    {        
        if (tipoDeJuegoAJugar == tipoDeJuego.multiJugador)
        {
            jugarMultijugador(fichaJugada);            
        }

        else if (tipoDeJuegoAJugar == tipoDeJuego.vsIA)
        {
            jugarVsIa(fichaJugada);            
        }       
    }

    void jugarMultijugador(Ficha fichaJugada)
    {
        if (configuracionDelJuego.jugardor1.juegaPrimero)
        {
            if (turno % 2 == 0)
            {
                fichaJugada.espacioDeDibujo.color = configuracionDelJuego.jugardor2.colorDelJugador;
                fichaJugada.espacioDeDibujo.sprite = configuracionDelJuego.jugardor2.spriteAJugar;
                fichaJugada.miEstado = Ficha.EstadoDeLaficha.jugadaPorEnemigo;
                asignarColorAlTurno(configuracionDelJuego.jugardor1.colorDelJugador);
            }
            else
            {
                fichaJugada.espacioDeDibujo.color = configuracionDelJuego.jugardor1.colorDelJugador;
                fichaJugada.espacioDeDibujo.sprite = configuracionDelJuego.jugardor1.spriteAJugar;
                fichaJugada.miEstado = Ficha.EstadoDeLaficha.jugadaPorJugador;
                asignarColorAlTurno(configuracionDelJuego.jugardor2.colorDelJugador);
            }
        }

        else if (configuracionDelJuego.jugardor2.juegaPrimero)
        {
            if (turno % 2 == 0)
            {
                fichaJugada.espacioDeDibujo.color = configuracionDelJuego.jugardor1.colorDelJugador;
                fichaJugada.espacioDeDibujo.sprite = configuracionDelJuego.jugardor1.spriteAJugar;
                fichaJugada.miEstado = Ficha.EstadoDeLaficha.jugadaPorJugador;
                asignarColorAlTurno(configuracionDelJuego.jugardor2.colorDelJugador);
            }
            else
            {
                fichaJugada.espacioDeDibujo.color = configuracionDelJuego.jugardor2.colorDelJugador;
                fichaJugada.espacioDeDibujo.sprite = configuracionDelJuego.jugardor2.spriteAJugar;
                fichaJugada.miEstado = Ficha.EstadoDeLaficha.jugadaPorEnemigo;
                asignarColorAlTurno(configuracionDelJuego.jugardor1.colorDelJugador);
            }
        }

        turno++;
        comprobarVictoria(fichaJugada.indice, fichaJugada.miEstado);
    }

    void jugarVsIa(Ficha fichaJugada)
    {
        turno++;
        if (fichaJugada.miEstado == Ficha.EstadoDeLaficha.jugadaPorIA)
        {
            fichaJugada.espacioDeDibujo.color = configuracionDelJuego.ia.colorDelJugador;
            fichaJugada.espacioDeDibujo.sprite = configuracionDelJuego.ia.spriteAJugar;
            comprobarVictoria(fichaJugada.indice, fichaJugada.miEstado);
        }

        else
        {
            fichaJugada.espacioDeDibujo.color = configuracionDelJuego.jugardor1.colorDelJugador;
            fichaJugada.espacioDeDibujo.sprite = configuracionDelJuego.jugardor1.spriteAJugar;
            fichaJugada.miEstado = Ficha.EstadoDeLaficha.jugadaPorJugador;
            comprobarVictoria(fichaJugada.indice, fichaJugada.miEstado);
            juegaLaIa();
        }
    }

    void comprobarVictoria(int fichaJugada, Ficha.EstadoDeLaficha fichaJugadaPor)
    {

        if (turno < 4)
        {
            return;
        }
        if (turno == 9)
        {
            asignarEmpate();
            return;
        }

        switch (fichaJugada)
        {
            case 0:
                comprobarFichas(0, 0, 0, fichaJugadaPor);
                break;

            case 1:
                comprobarFichas(0, 1, -1, fichaJugadaPor);
                break;

            case 2:
                comprobarFichas(0, 2, 2, fichaJugadaPor);
                break;

            case 3:
                comprobarFichas(3, 0, -1, fichaJugadaPor);
                break;

            case 4:
                comprobarFichas(3, 1, 5, fichaJugadaPor);
                break;

            case 5:
                comprobarFichas(3, 2, -1, fichaJugadaPor);
                break;

            case 6:
                comprobarFichas(6, 0, 2, fichaJugadaPor);
                break;

            case 7:
                comprobarFichas(6, 1, -1, fichaJugadaPor);
                break;

            case 8:
                comprobarFichas(6, 2, 0, fichaJugadaPor);
                break;
        }
    }   

    void comprobarFichas(int fila, int columna, int diagonal, Ficha.EstadoDeLaficha jugadaPorQuien)
    {
        if (diagonal != -1)
        {
            if (diagonal == 5)
            {
                comprobarDiagonales(0, jugadaPorQuien);
                comprobarDiagonales(2, jugadaPorQuien);
            }
            comprobarDiagonales(diagonal, jugadaPorQuien);
        }

        comprobarColumnas(columna, jugadaPorQuien);
        comprobarFilas(fila, jugadaPorQuien);
    }

    void comprobarDiagonales(int indice, Ficha.EstadoDeLaficha quienJugoLaFicha)
    {
        if (indice == 0)
        {
            if (fichasDeJuego[indice].miEstado == quienJugoLaFicha &&
            fichasDeJuego[indice + 4].miEstado == quienJugoLaFicha &&
            fichasDeJuego[indice + 8].miEstado == quienJugoLaFicha)
            {                
                asignarGanador(quienJugoLaFicha);
            }
        }

        if (indice == 2)
        {
            if (fichasDeJuego[indice].miEstado == quienJugoLaFicha &&
                fichasDeJuego[indice + 2].miEstado == quienJugoLaFicha &&
                fichasDeJuego[indice + 4].miEstado == quienJugoLaFicha)
            {                
                asignarGanador(quienJugoLaFicha);
            }
        }        
    }

    void comprobarFilas(int indice, Ficha.EstadoDeLaficha quienJugoLaFicha)
    {
        if (fichasDeJuego[indice].miEstado == quienJugoLaFicha &&
            fichasDeJuego[indice + 1].miEstado == quienJugoLaFicha &&
            fichasDeJuego[indice + 2].miEstado == quienJugoLaFicha)
        {            
            asignarGanador(quienJugoLaFicha);
        }
    }

    void comprobarColumnas(int indice, Ficha.EstadoDeLaficha quienJugoLaFicha)
    {
        if (fichasDeJuego[indice].miEstado == quienJugoLaFicha &&
            fichasDeJuego[indice + 3].miEstado == quienJugoLaFicha &&
            fichasDeJuego[indice + 6].miEstado == quienJugoLaFicha)
        {            
            asignarGanador(quienJugoLaFicha);
        }
    }

    void asignarGanador(Ficha.EstadoDeLaficha ganador)
    {
        if (ganador == Ficha.EstadoDeLaficha.jugadaPorJugador)
        {
            contolUI.textoDeResultado.text = "Gana el Jugador : " + configuracionDelJuego.jugardor1.colorDelJugador.ToString();
            contolUI.activarReinicioDeJuego();
        }
        else if (ganador == Ficha.EstadoDeLaficha.jugadaPorEnemigo)
        {
            contolUI.textoDeResultado.text = "Gana el Jugador : " + configuracionDelJuego.jugardor2.colorDelJugador.ToString();
            contolUI.activarReinicioDeJuego();
        }
        else if (ganador == Ficha.EstadoDeLaficha.jugadaPorIA)
        {
            contolUI.textoDeResultado.text = "Derrota";
            contolUI.activarReinicioDeJuego();
        }
    }

    void asignarEmpate()
    {
        contolUI.textoDeResultado.text = "Empate";
        contolUI.activarReinicioDeJuego();        
    }
}
