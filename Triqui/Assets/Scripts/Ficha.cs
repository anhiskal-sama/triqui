﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Ficha : MonoBehaviour
{
    public Sprite colorDeTiza;
    private Button espacioDeTrabajo;
    public Image espacioDeDibujo;
    public int indice;

    public enum EstadoDeLaficha {sinJugar = 0, jugadaPorJugador = 1, jugadaPorIA = 2, jugadaPorEnemigo = 3 }
    public EstadoDeLaficha miEstado;

    public bool jugadaPorElJugador;

    private bool laFichaFueUsada;
    public Juego referenciDelJuego;

    
    Color colorDeLaFicha;

    public ConfiguracionDelJuego referenciaDeLaConfiguracion;

	// Use this for initialization
	void Start ()
    {
        reiniciarFicha();        
	}

    public void reiniciarFicha()
    {
        laFichaFueUsada = false;
        espacioDeTrabajo = GetComponent<Button>();
        espacioDeDibujo = GetComponent<Image>();
        espacioDeDibujo.sprite = null;
        espacioDeDibujo.color = Color.clear;
        miEstado = EstadoDeLaficha.sinJugar;
        //asigna la sprite
    }

    public void activarFicha()
    {
        if (!laFichaFueUsada)
        {
            laFichaFueUsada = true;
            referenciDelJuego.activarFicha(this);
        }
        //espacioDeTrabajo.spriteState = colorDeTiza;
    }
}
