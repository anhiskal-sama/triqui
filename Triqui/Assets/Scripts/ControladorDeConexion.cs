﻿using UnityEngine;
using System.Collections;
using System.IO;
using LitJson;

public class ControladorDeConexion : MonoBehaviour
{
    private string urlDeBusqueda = "http://jsonplaceholder.typicode.com/users.json";
    private string datosDelJuego;
    public JsonData itemsDeDatos;

    public DatosDescargadosDelJugador datosDelJugador;

    private string idS;
    private string[] informacionDatosPersonles = new string[5];    
    private string[] informacionDeDirrecion = new string[4];    
    private string[] informacionGeo = new string[2];    
    private string[] informacionDeEmpresa = new string[3];

    private int indiceJugadorAleatorio;   

    float[] coordenadas = new float[2];

    string temp;


    void Start()
    {        
        descargarArchivo();
        desempaquetarArchivo();         
    }

    void descargarArchivo()
    {
        datosDelJuego = File.ReadAllText(Application.dataPath + "/Informacion/users.json");
    }

    void desempaquetarArchivo()
    {
        itemsDeDatos = JsonMapper.ToObject(datosDelJuego);
    }

    public void generarIndiceAleatorio()
    {
        descargarArchivo();
        desempaquetarArchivo();
        indiceJugadorAleatorio = Random.Range(0, itemsDeDatos.Count);
        cargarInformacion(indiceJugadorAleatorio);           
    }

    public void cargarInformacion(int id)
    {
        //JsonData datosMapeados = JsonMapper.ToObject(archivoCargado);       

        if (true)
        {
            idS = itemsDeDatos[id]["id"].ToString();

            temp = itemsDeDatos[id]["name"].ToString();
            informacionDatosPersonles[0] = temp;

            temp = itemsDeDatos[id]["username"].ToString();
            informacionDatosPersonles[1] = temp;

            temp = itemsDeDatos[id]["email"].ToString();
            informacionDatosPersonles[2] = temp;

            temp = itemsDeDatos[id]["phone"].ToString();
            informacionDatosPersonles[3] = temp;

            temp = itemsDeDatos[id]["website"].ToString();
            informacionDatosPersonles[4] = temp;

            for (int datosDeDirrecion = 0; datosDeDirrecion < itemsDeDatos[id]["address"].Count - 1; datosDeDirrecion++)
            {
                temp = itemsDeDatos[id]["address"][datosDeDirrecion].ToString();
                informacionDeDirrecion[datosDeDirrecion] = temp;
            }

            for (int datosGeo = 0; datosGeo < itemsDeDatos[id]["address"][4].Count; datosGeo++)
            {
                temp = itemsDeDatos[id]["address"][4][datosGeo].ToString();
                informacionGeo[datosGeo] = temp;
                //coordenadas[datosGeo] = Convert.ToSingle(informacionGeo[datosGeo]);
            }

            for (int datosEmpresa = 0; datosEmpresa < itemsDeDatos[id]["company"].Count; datosEmpresa++)
            {
                temp = itemsDeDatos[id]["company"][datosEmpresa].ToString();
                informacionDeEmpresa[datosEmpresa] = temp;
            }

            datosDelJugador = new DatosDescargadosDelJugador(id, informacionDatosPersonles, informacionDeDirrecion, informacionGeo, informacionDeEmpresa);
        }

        else
        {
            Debug.Log("Error en el archivo");
        }        
    }

    IEnumerator buscarJugadores()
    {
        Debug.Log("IniciaLaBusquedaDeJugadores");

        WWWForm form = new WWWForm();         
        WWW consulta = new WWW(urlDeBusqueda, form);
        yield return consulta;


        if (consulta.error != null)
        {
            Debug.Log(consulta.error);
            errorConLaConexion();
        }
            
        else
        {
            string texto = consulta.text;
            //texto.Remove((texto.Length - 1), 1);
            Debug.Log(consulta.text);
        }
     }

    void errorConLaConexion()
    {        
    }

}
