﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ControladorDelUI : MonoBehaviour
{
    public GameObject menuPrincipalPanel;
    public GameObject configuracionPanel;
    public GameObject espacioDeJuego;
    public GameObject reiniciarJuegoPanel;
    public GameObject seleccionDeModoDeJuegoPanel;
    public Text textoDeResultado;

    public Juego referenciaAlJuego;

    public void activarMenuPrincipal()
    {
        desactivarTodosLosPaneles();
        menuPrincipalPanel.SetActive(true);
    }

    public void activarEspacioDeJuego()
    {
        referenciaAlJuego.iniciarJuego();
        desactivarTodosLosPaneles();
        reiniciarJuegoPanel.SetActive(false);
        espacioDeJuego.SetActive(true);
    }

    public void activarConfiguracion()
    {
        desactivarTodosLosPaneles();
        configuracionPanel.SetActive(true);
    }

    public void seleccionModoMultiJugador()
    {
        referenciaAlJuego.activarModoMultiJugador();
        activarEspacioDeJuego();
    }

    public void seleccionModoVsIA()
    {
        referenciaAlJuego.activarModoVsIA();
        activarEspacioDeJuego();
    }

    public void activarSeleccionModoDeJuego()
    {
        desactivarTodosLosPaneles();
        seleccionDeModoDeJuegoPanel.SetActive(true);
    }

    public void activarReinicioDeJuego()
    {
        reiniciarJuegoPanel.SetActive(true);
    }

    public void desactivarTodosLosPaneles()
    {
        menuPrincipalPanel.SetActive(false);
        espacioDeJuego.SetActive(false);
        configuracionPanel.SetActive(false);
        seleccionDeModoDeJuegoPanel.SetActive(false);
    }

    public void salirDelJuego()
    {

    }
}
