﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class DatosDescargadosDelJugador
{
    public DatosDescargadosDelJugador(int idCargado, string[] datosPropios, string[] datosDirrecion, string[] datosGeo, string[] datosCompañia)
    {
        this.id = idCargado;

        this.nombre = datosPropios[0];
        this.nombreDeUsuario = datosPropios[1];
        this.correo = datosPropios[2];
        this.telefono = datosPropios[3];
        this.sitioWeb = datosPropios[4];

        this.dirrecion = new Address(datosDirrecion[0], datosDirrecion[1], datosDirrecion[2], datosDirrecion[3] , datosGeo);
        this.compañia = new Compañia(datosCompañia[0], datosCompañia[1], datosCompañia[2]);
    }

    public int id;
    public string nombre;
    public string nombreDeUsuario;
    public string correo;    

    public string telefono;
    public string sitioWeb;

    public Address dirrecion;
    public Compañia compañia;
}

[System.Serializable]
public class Compañia
{
    public Compañia(string compañia, string catchPh, string bsCompañia)
    {
        this.nombreDeLaCompañia = compañia;
        this.catchPhrase = catchPh;
        this.bs = bsCompañia;
    }

    public string nombreDeLaCompañia;
    public string catchPhrase;
    public string bs;
}

[System.Serializable]
public class Address
{
    public Address(string calleCargada, string suiteCargada, string ciudadCargada, string codigoPostalCargado, string[] datosGeo)
    {
        this.calle = calleCargada;
        this.suite = suiteCargada;
        this.ciudad = ciudadCargada;
        this.codigoPostal = codigoPostalCargado;

        this.miGeo = new Geo(datosGeo[0], datosGeo[1]);
    }

    public string calle;
    public string suite;
    public string ciudad;
    public string codigoPostal;
    public Geo miGeo;
}

[System.Serializable]
public class Geo
{
    public Geo(string latDescagada, string lngDescagada)
    {
        this.lat = latDescagada;
        this.lng = lngDescagada;
    }
    public string lat;
    public string lng;
}
