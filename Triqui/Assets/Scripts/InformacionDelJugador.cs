﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class InformacionDelJugador : MonoBehaviour
{
    public Color colorDelJugador;
    public enum Simbolos { X = 1, O = 2, }
    public Simbolos miSimbolo;
    public Ficha.EstadoDeLaficha simboloDelJugador;
    public bool juegaPrimero;
    public Sprite spriteAJugar;

    public string nombreDelJugador;

}
