﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ConfiguracionDelJuego : MonoBehaviour
{
    public InformacionDelJugador jugardor1 = new InformacionDelJugador();
    public InformacionDelJugador jugardor2 = new InformacionDelJugador();    
    public InformacionDelJugador ia = new InformacionDelJugador();  
      
    public InformacionDelJugador.Simbolos simbolosDelJuego;    
      
    public Color[] coloresDeSeleccion = {Color.red, Color.blue, Color.white, Color.yellow };
    public Sprite spriteX;
    public Sprite spriteO;

    public GameObject seleccionDeColorPanel;

    public Dropdown simboloSeleccionado;
    public Dropdown simboloSeleccionadoEnemigo;
    public Image imagenDelBotonDelColor;
    public Image imagenDelBotonDelColorEnemigo;
    public Scrollbar volumenDeLosEfectos;
    public Scrollbar volumenDeLaMusica;

    int jugadorSeleccionando;

    void Start()
    {
        configuracionPorDefecto();
    }

    public void configuracionPorDefecto()
    {
        jugardor1.colorDelJugador = coloresDeSeleccion[0];
        jugardor1.spriteAJugar = spriteO;
        jugardor1.miSimbolo = InformacionDelJugador.Simbolos.X;
        jugardor1.simboloDelJugador = Ficha.EstadoDeLaficha.jugadaPorJugador;
        simboloSeleccionado.value = 0;

        jugardor2.colorDelJugador = coloresDeSeleccion[1];
        jugardor2.spriteAJugar = spriteX;
        jugardor2.miSimbolo = InformacionDelJugador.Simbolos.O;
        jugardor2.simboloDelJugador = Ficha.EstadoDeLaficha.jugadaPorEnemigo;
        simboloSeleccionadoEnemigo.value = 1;

        ia.colorDelJugador = coloresDeSeleccion[2];
        ia.spriteAJugar = spriteX;
        ia.miSimbolo = InformacionDelJugador.Simbolos.O;
        ia.simboloDelJugador = Ficha.EstadoDeLaficha.jugadaPorIA;

        asignarColoresALosBotones();
    }

    public Color colorAJugarDeLaIa(Color colorDelJugador)
    {
        Color nuevoColorDeLaIA;
        do
        {
            nuevoColorDeLaIA = coloresDeSeleccion[Random.Range(0, 3)];
        } while (jugardor1.colorDelJugador == nuevoColorDeLaIA);        
        
        return nuevoColorDeLaIA;
    }

    public InformacionDelJugador.Simbolos simboloAsignadoALaIA(InformacionDelJugador.Simbolos simboloDelJugador)
    {
        if (jugardor1.miSimbolo == InformacionDelJugador.Simbolos.X)
        {
            return InformacionDelJugador.Simbolos.O;
        }
        else
        {
            return InformacionDelJugador.Simbolos.X;
        }        
    }

    public void seleccionarColorDelJugador(int indiceDelColorSeleccionado)
    {
        Color temp;        
        if (jugadorSeleccionando == 1)
        {
            if (coloresDeSeleccion[indiceDelColorSeleccionado] == jugardor2.colorDelJugador)
            {
                temp = jugardor1.colorDelJugador;
                jugardor1.colorDelJugador = coloresDeSeleccion[indiceDelColorSeleccionado];
                jugardor2.colorDelJugador = temp;
            }
            else
            {
                jugardor1.colorDelJugador = coloresDeSeleccion[indiceDelColorSeleccionado];                
            }
            ia.colorDelJugador = colorAJugarDeLaIa(jugardor1.colorDelJugador);         
        }

        if (jugadorSeleccionando == 2)
        {
            if (coloresDeSeleccion[indiceDelColorSeleccionado] == jugardor1.colorDelJugador)
            {
                temp = jugardor2.colorDelJugador;
                jugardor2.colorDelJugador = coloresDeSeleccion[indiceDelColorSeleccionado];
                jugardor1.colorDelJugador = temp;                
            }
            else
            {
                jugardor2.colorDelJugador = coloresDeSeleccion[indiceDelColorSeleccionado];                
            }
        }
        asignarColoresALosBotones();        
        ActivarODesactiarPanelDeColores(false);
    }

    public void asignarColoresALosBotones()
    {
        imagenDelBotonDelColor.color = jugardor1.colorDelJugador;
        imagenDelBotonDelColorEnemigo.color = jugardor2.colorDelJugador;
    }

    public void seleccionaSimbolo(int jugador)
    {
        jugadorSeleccionando = jugador;

        if (jugadorSeleccionando == 1)
        {
            switch (simboloSeleccionado.value)
            {
                case 0:
                    asignarSpriteO();
                    simboloSeleccionadoEnemigo.value = 1;                  
                    break;

                case 1:
                    asignarSpriteX();
                    simboloSeleccionadoEnemigo.value = 0;                 
                    break;               
            }
        }

        if (jugadorSeleccionando == 2)
        {
            switch (simboloSeleccionadoEnemigo.value)
            {
                case 0:
                    asignarSpriteX();
                    simboloSeleccionado.value = 1;
                    break;

                case 1:
                    asignarSpriteO();
                    simboloSeleccionado.value = 0;
                    break;
            }
        }        
    }

    void asignarSpriteX()
    {
        jugardor1.spriteAJugar = spriteX;
        jugardor2.spriteAJugar = spriteO;
        ia.spriteAJugar = spriteO;
    }

    void asignarSpriteO()
    {
        jugardor1.spriteAJugar = spriteO;
        jugardor2.spriteAJugar = spriteX;
        ia.spriteAJugar = spriteX;
    }

    void asignarSimbolo(InformacionDelJugador infoJugador, InformacionDelJugador.Simbolos simboloAASignar)
    {
        infoJugador.miSimbolo = simboloAASignar;
    }

    void asignarColor(InformacionDelJugador infoJugador, Color colorAAsignar)
    {
        infoJugador.colorDelJugador = colorAAsignar;
    }

    void asignarSimboloX()
    {
        simbolosDelJuego = InformacionDelJugador.Simbolos.X;
    }

    void asignarSimboloO()
    {
        simbolosDelJuego = InformacionDelJugador.Simbolos.O;
    }

    public void ActivarODesactiarPanelDeColores(bool estado)
    {
        seleccionDeColorPanel.SetActive(estado);
    }

    public void Seleccionando(int jugador)
    {
        jugadorSeleccionando = jugador;
        ActivarODesactiarPanelDeColores(true);
    }
}
