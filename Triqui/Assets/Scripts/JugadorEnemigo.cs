﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class JugadorEnemigo : MonoBehaviour
{
    //public bool[] estadoDelJuego;   

    
    public Ficha[] estadoDelJuego;    
    public bool elJugadorJugoElSlotCentral = false;    
    int numeroDelTurno;

    public void jugar(int turno)
    {
        jugarRandom();        
    }

    public void jugarRandom()
    {
        int movimientos = cantidadDePosiblesJugadas(estadoDelJuego);
        int[] indices = posiblesJugadas(estadoDelJuego);
        int indiceAJugar = indices[Random.Range(0, movimientos)];
        estadoDelJuego[indiceAJugar].miEstado = Ficha.EstadoDeLaficha.jugadaPorIA;
        estadoDelJuego[indiceAJugar].activarFicha();
    }

    public int cantidadDePosiblesJugadas(Ficha[] tablero)
    {
        int cantidaDePosiblesJugadas = 0;
        foreach (Ficha ficha in estadoDelJuego)
        {
            if (ficha.miEstado == Ficha.EstadoDeLaficha.sinJugar)
            {
                cantidaDePosiblesJugadas++;
            }
        }
        return cantidaDePosiblesJugadas;
    }

    public int[] posiblesJugadas(Ficha[] tablero)
    {
        int[] indicesDeJugadasPosibles = new int[cantidadDePosiblesJugadas(tablero)];
        int indices = 0;
        for(int i = 0; i < 9; i++)
        {
            if (tablero[i].miEstado == Ficha.EstadoDeLaficha.sinJugar)
            {
                indicesDeJugadasPosibles[indices] = i;
                indices++;
            }
        }
        return indicesDeJugadasPosibles;
    } 

    public Ficha.EstadoDeLaficha tableroTerminado(Ficha[] tablero)
    {
        //Filas
        if (tablero[0] == tablero[1] && tablero[0] == tablero[2] && tablero[0].miEstado != Ficha.EstadoDeLaficha.sinJugar)
        {
            return tablero[0].miEstado;
        }
        else if (tablero[3] == tablero[4] && tablero[3] == tablero[5] && tablero[3].miEstado != Ficha.EstadoDeLaficha.sinJugar)
        {
            return tablero[3].miEstado;
        }
        else if (tablero[6] == tablero[7] && tablero[6] == tablero[8] && tablero[6].miEstado != Ficha.EstadoDeLaficha.sinJugar)
        {
            return tablero[6].miEstado;
        }

        //Columnas
        else if (tablero[0] == tablero[3] && tablero[0] == tablero[6] && tablero[0].miEstado != Ficha.EstadoDeLaficha.sinJugar)
        {
            return tablero[0].miEstado;
        }
        else if (tablero[1] == tablero[4] && tablero[1] == tablero[7] && tablero[1].miEstado != Ficha.EstadoDeLaficha.sinJugar)
        {
            return tablero[1].miEstado;
        }
        else if (tablero[2] == tablero[5] && tablero[2] == tablero[8] && tablero[8].miEstado != Ficha.EstadoDeLaficha.sinJugar)
        {
            return tablero[2].miEstado;
        }

        //Diagonales
        else if (tablero[0] == tablero[5] && tablero[0] == tablero[8] && tablero[0].miEstado != Ficha.EstadoDeLaficha.sinJugar)
        {
            return tablero[0].miEstado;
        }

        else if (tablero[2] == tablero[4] && tablero[2] == tablero[6] && tablero[2].miEstado != Ficha.EstadoDeLaficha.sinJugar)
        {
            return tablero[2].miEstado;
        }

        //En caso de que no tenga un ganador
        return Ficha.EstadoDeLaficha.sinJugar;
    }



}
